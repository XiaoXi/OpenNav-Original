<?php
/**
 * name: 全局入口文件
 * author: XiaoXi<admin@soraharu.com>
 */

// 载入 PHP 库
// GoogleAuthenticator
require_once("./Class/PHPGangsta/GoogleAuthenticator.php");

// 关闭 PHP 错误提示
//error_reporting(E_ALL^E_NOTICE^E_WARNING^E_DEPRECATED);
// 设置 Contect-Type 请求头为 text/html
header("Content-Type: text/html; charset=utf-8");
// 获取控制器
$controller = @$_GET['c'];
// 进行过滤
$controller = strip_tags($controller);
// 载入配置文件
// 如果配置文件不存在，则载入初始化文件
if (!file_exists("./Data/config.php")) {
	include_once("./Controller/init.php");
}
// 检查数据库是否存在，不存在则复制数据库
if (!file_exists('./Data/opennav.db3')) {
	if (!copy('./db/opennav.sample.db3', './Data/opennav.db3')) {
		exit("数据库初始化失败，请检查目录权限！");
	}
}

// 载入配置文件
require("./Data/config.php");

// 根据不同的请求载入不同的方法
// 如果没有请求控制器
if ((!isset($controller)) || ($controller == "")) {
	// 载入主页
    include_once("./Controller/index.php");
} else {
	// 对请求参数进行过滤，同时检查文件是否存在
	$controller = str_replace('\\', '/', $controller);
	$pattern = "%\./%";
	if (preg_match_all($pattern, $controller)) {
		exit('非法请求！');
	}
	// 控制器文件
	$controller_file = "./Controller/" . $controller . ".php";
	if (file_exists($controller_file)) {
		include_once($controller_file);
	} else {
		exit('控制器不存在！');
	}
}
