<?php
/**
 * name: TOTP 二维码生成接口
 * author: XiaoXi<admin@soraharu.com>
 */

// 载入辅助函数
require('./Functions/helper.php');

if (is_login()) {
    $google_authenticator = new PHPGangsta_GoogleAuthenticator();
    $totp_secret = $site_setting['totp_secret'];
    $totp_url = $google_authenticator->getQRCodeGoogleUrl("OpenNav", $totp_secret);
    header('Content-Type: text/html; charset=utf-8');
    echo("<img src='" . $totp_url . "' />");
    // echo("<img src='https://qr.png.pub/v1/?text=" . $totp_url . "' />");
} else {
    header("Location: ./index.php?c=login");
}
