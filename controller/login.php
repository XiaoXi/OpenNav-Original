<?php
/**
 * name: 登录入口
 * author: XiaoXi<admin@soraharu.com>
 */

// 载入辅助函数
require('./Functions/helper.php');

$username = $site_setting['user'];
$password = $site_setting['password'];
$totp_secret = $site_setting['totp_secret'];

$ip = getIP();
// 如果认证通过，直接跳转到后台管理
$key = md5($username . $password . 'opennav' . $_SERVER['HTTP_USER_AGENT']);
// 获取 cookie
$cookie = $_COOKIE['key'];

// 如果已经登录，直接跳转
if (is_login()) {
	header('location:./index.php?c=admin');
	exit;
}

// 登录检查
if ($_GET['check'] == 'login') {
	// 实例化 GoogleAuthenticator 库
	$google_authenticator = new PHPGangsta_GoogleAuthenticator();

	$user = htmlspecialchars(trim($_POST['user']));
	$pass = htmlspecialchars(trim($_POST['password']));
	$totp_token = htmlspecialchars(trim($_POST['totptoken']));

	$totp_check_result = $google_authenticator->verifyCode($totp_secret, $totp_token, 2);

	header('Content-Type: application/json; charset=utf-8');
	if (($user === $username) && ($pass === $password)) {
		$key = md5($username . $password . 'opennav' . $_SERVER['HTTP_USER_AGENT']);
		// 开启 httponly 支持
		setcookie("key", $key, time() + 30 * 24 * 60 * 60, "/", null, false, true);
		$data = [
			'code' => 0,
			'msg' => '密码登录成功！'
		];
	} elseif ($totp_check_result) {
		$key = md5($username . $password . 'opennav' . $_SERVER['HTTP_USER_AGENT']);
		// 开启 httponly 支持
		setcookie("key", $key, time() + 24 * 60 * 60, "/", null, false, true);
		$data = [
			'code' => 0,
			'msg' => 'TOTP 登录成功！'
		];
	} else {
		$data = [
			'code' => -1012,
			'err_msg' => '登录信息错误！'
		];
	}
	exit(json_encode($data));
}
// 如果 cookie 的值和计算的 key 不一致，则没有权限

// 载入后台登录模板
require('./templates/admin/login.php');
