<?php
// 载入 PHP 库
// Medoo
require_once("./Class/Medoo/Medoo.php");
use Medoo\Medoo;
$db = new Medoo([
    "database_type" => "sqlite",
    "database_file" => "./Data/opennav.db3"
]);

// 用户名
define("USER", "{username}");
// 密码
define("PASSWORD", "{password}");
// 邮箱，用于后台 Gravatar 头像显示
define("EMAIL", "{email}");
// TOTP SECRET
define("TOTP_SECRET", "{totpsecret}");
// TOKEN
define("TOKEN", "opennav");
// 主题
define("TEMPLATE", "default");

// 站点信息
$site_setting = [];
// 标题
$site_setting['title'] = "OpenNav";
// 文字 Logo
$site_setting['logo'] = "OpenNav";
// 关键词
$site_setting['keywords'] = "OpenNav,开源导航,开源书签,个人导航,个人书签";
// 描述
$site_setting['description'] = "";

// 以下请勿更改
$site_setting['user'] = USER;
$site_setting['password'] = PASSWORD;
$site_setting['totp_secret'] = TOTP_SECRET;
