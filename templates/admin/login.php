<!DOCTYPE html>
<html lang="zh-cmn-Hans">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="./templates/admin/static/css/new.css" />
		<link rel="stylesheet" href="./node_modules/layui/dist/css/layui.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
		<title>登录 OpenNav</title>
		<script>
			window.onload = function () {
				document.querySelector(".login").style.opacity = 1;
			}
		</script>
	</head>
	<body class="login">
		<div class="root">
			<section class="left">
				<img class="cover" src="./templates/admin/static/image/bg.png" />
			</section>
			<section class="right">
				<!-- PC 版的样式 -->
				<h2>OpenNav Portal</h2>
				<div class="login_frame">
					<div class="login_box">
						<h4>管理员登录</h4>
						<h6>欢迎回到 OpenNav 门户！</h6>
						<form action="" method="post">
							<div class="inp">
								<span class="label">用户名</span>
								<input type="text" id="user" name="user" placeholder="请输入用户名" />
							</div>
							<div class="inp">
								<span class="label">密码</span>
								<input type="password" id="password" name="password" placeholder="请输入密码" />
							</div>
							<div class="inp">
								<span class="label">TOTP</span>
								<input type="number" id="totptoken" name="totptoken" placeholder="不理解请留空" />
							</div>
							<div class="submit">
								<input type="submit" lay-submit lay-filter="new_login" class="submit" value="登录" />
							</div>
						</form>
					</div>
				</div>
			</section>
		</div>
		<div class="mobile">
			<!-- 手机版的样式 -->
			<h1>OpenNav Portal</h1>
			<form action="" method="post">
				<div class="inp">
					<span class="label">用户名</span>
					<input type="text" id="m_user" name="user" placeholder="请输入用户名" />
				</div>
				<div class="inp">
					<span class="label">密码</span>
					<input type="password" id="m_password" name="password" placeholder="请输入密码" />
				</div>
				<div class="inp">
					<span class="label">TOTP</span>
					<input type="number" id="m_totptoken" name="totptoken" placeholder="不理解请留空" />
				</div>
				<div class="submit">
					<input type="submit" lay-submit lay-filter="new_mobile_login" class="submit" value="登录" />
				</div>
			</form>
		</div>
		<footer>© <?php echo date("Y"); ?> Powered by <a style="color: #FFFFFF; padding-left: 6px;" href="https://github.com/yanranxiaoxi/OpenNav" target="_blank" title="OpenNav"> OpenNav</a></footer>

		<script>
			// 弹出框
			function alt(text) {
				const t = document.createElement("div")
				t.innerText = text;
				Object.assign(t.style, {
					position: 'fixed',
					maxWidth: '300px',
					top: '50px',
					left: '0px',
					right: '0px',
					margin: '0 auto',
					color: '#000',
					background: '#fff',
					boxShadow: '0px 3px 4px rgba(197, 197, 197, 0.115)',
					padding: '15px 20px',
					borderRadius: '8px',
					transition: 'all .5s',
					opacity: 0,
					transform: 'translateY(-10px)'
				})
				document.body.append(t)
				setTimeout(_ => {
					t.style.transform = 'translateY(10px)'
					t.style.opacity = 1;
				}, 100)
				setTimeout(_ => {
					t.style.transform = 'translateY(-10px)'
					t.style.opacity = 0;
				}, 3000)
			}
		</script>
		<script src="./node_modules/jquery/dist/jquery.min.js"></script>
		<script src="./node_modules/layui/dist/layui.js"></script>
		<script src="./templates/admin/static/embed.js"></script>
	</body>
</html>
