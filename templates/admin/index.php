<?php include_once('header.php'); ?>
<?php include_once('left.php'); ?>

<div class="layui-body place-holder">
	<!-- 内容主体区域 -->
	<div style="padding: 15px;">
		<div class="layui-container" style = "margin-top: 2em;">

			<div class="layui-row layui-col-space18">
				<div class="layui-col-lg4">
					<div class="admin-msg">当前版本：<span id="current_version"><?php echo file_get_contents('./version.txt'); ?></span>
					<span id="update_msg" style="display: none;">（<a style="color: #FF5722;" href="https://gitlab.soraharu.com/XiaoXi/OpenNav/-/releases" title="下载最新版 OpenNav" target="_blank" id="current_version">有可用更新</a>）</span>
				</div>
			</div>
			<div class="layui-col-lg4">
				<div class = "admin-msg">
					最新版本：<span><span id="getting">获取中...</span><a href="https://gitlab.soraharu.com/XiaoXi/OpenNav/-/releases" title="下载最新版 OpenNav" target="_blank" id="latest_version"></a></span>
					(<a href="./index.php?c=admin&page=setting/subscribe" title="订阅后可一键更新">一键更新</a>)
				</div>
			</div>
			<div class="layui-col-lg4">
				<div class="admin-msg">时基验证：<a href="./index.php?c=totpQRCode" rel="nofollow" target="_blank">点击查看 TOTP 二维码</a></div>
			</div>
			<div class="layui-col-lg4">
				<div class="admin-msg">项目仓库：<a href="https://github.com/yanranxiaoxi/OpenNav" rel="nofollow" target="_blank">yanranxiaoxi/OpenNav</a></div>
			</div>
			<div class="layui-col-lg4">
				<div class="admin-msg">作者博客：<a href="https://tech.soraharu.com/" rel="nofollow" target="_blank">https://tech.soraharu.com/</a></div>
			</div>
			<div class="layui-col-lg4">
				<div class="admin-msg">打赏咖啡：<a href="https://www.buymeacoffee.com/yanranxiaoxi" rel="nofollow" target="_blank">By Me A Coffee</a></div>
			</div>

			<!-- 日志输出窗口 -->
			<div class="layui-col-lg12">
				<p><h3 style="padding-bottom: 1em;">日志输出</h3></p>
				<textarea id="console_log" name="desc" rows="20" placeholder="日志输出控制台" class="layui-textarea" readonly="readonly"></textarea>
			</div>
			<!-- 日志输出窗口 END -->

			</div>
		</div>
	</div>
</div>

<?php include_once('footer.php'); ?>
<script>
	check_db_down();
	check_weak_password();
	get_sql_update_list();
	get_latest_version();
</script>
