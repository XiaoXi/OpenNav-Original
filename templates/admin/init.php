<?php
$google_authenticator = new PHPGangsta_GoogleAuthenticator();
$totp_secret = $google_authenticator->createSecret();
?>

<!DOCTYPE html>
<html lang="zh-cmn-Hans" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<title>初始化 OpenNav</title>
		<meta name="author" content="XiaoXi<admin@soraharu.com>" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<link rel="stylesheet" href="./node_modules/layui/dist/css/layui.css" />
		<link rel="stylesheet" href="./templates/admin/static/style.css" />
		<style>
			body {
				/* background: url(./templates/admin/static/bg.jpg); */
				background-color: rgba(0, 0, 51, 0.8);
			}
		</style>
	</head>
	<body>
		<div class="layui-container">
			<div class="layui-row">
				<div class="login-logo">
					<h1>初始化 OpenNav</h1>
				</div>
				<div class="layui-col-lg6 layui-col-md-offset3" style ="margin-top: 4em;">
					<form class="layui-form layui-form-pane" action="">

						<div class="layui-form-item">
							<label class="layui-form-label">用户名</label>
							<div class="layui-input-block">
								<input type="text" name="username" required lay-verify="required" placeholder="3-32 位的字母或数字" autocomplete="off" class="layui-input" />
							</div>
						</div>

						<div class="layui-form-item">
							<label class="layui-form-label">密码</label>
							<div class="layui-input-block">
								<input type="password" name="password" required lay-verify="required" placeholder="6-128 位字母、数字或 !@#$%^&*.() 字符" autocomplete="off" class="layui-input" />
							</div>
						</div>

						<div class="layui-form-item">
							<label class="layui-form-label">确认密码</label>
							<div class="layui-input-block">
								<input type="password" name="password2" required lay-verify="required" placeholder="6-128 位字母、数字或 !@#$%^&*.() 字符" autocomplete="off" class="layui-input" />
							</div>
						</div>

						<div class="layui-form-item">
							<label class="layui-form-label">电子邮箱</label>
							<div class="layui-input-block">
								<input type="email" name="email" placeholder="可选，用于获取 Gravatar 头像" autocomplete="off" class="layui-input" />
							</div>
						</div>

						<div class="layui-form-item" style="display: none;">
							<label class="layui-form-label">TOTP Secret</label>
							<div class="layui-input-block">
								<input value="<?php echo $totp_secret; ?>" type="text" name="totpsecret" placeholder="TOTP Secret" autocomplete="off" class="layui-input" disabled />
							</div>
						</div>

						<div class="layui-form-item">
							<button class="layui-btn" lay-submit lay-filter="init_opennav" style="width: 100%;">设置</button>
						</div>

					</form>
				</div>
			</div>
		</div>

		<script src="./node_modules/jquery/dist/jquery.min.js"></script>
		<script src="./node_modules/layui/dist/layui.js"></script>
		<script src="./templates/admin/static/embed.js"></script>
	</body>
</html>
