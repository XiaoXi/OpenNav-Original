
			<div class="layui-footer">
				<!-- 底部固定区域 -->
				© <?php echo date("Y"); ?> Powered by <a target="_blank" href="https://github.com/yanranxiaoxi/OpenNav" title="OpenNav" rel="nofollow">OpenNav</a>. The author is <a href="https://soraharu.com/" target="_blank">XiaoXi</a>.
			</div>
		</div>

		<script src="./node_modules/jquery/dist/jquery.min.js"></script>
		<script src="./node_modules/layui/dist/layui.js"></script>
		<script src="./static/js/md5.min.js"></script>
		<script src="./templates/admin/static/embed.js?v=<?php echo $version; ?>"></script>
	</body>
</html>
